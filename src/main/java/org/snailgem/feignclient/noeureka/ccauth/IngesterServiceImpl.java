package org.snailgem.feignclient.noeureka.ccauth;

import org.snailgem.feignclient.ccauth.feedback.dto.FeedbackToIngest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngesterServiceImpl implements IngesterService {
    @Autowired
    private FeedbackClient feedbackClient;

    @Override
    public List <String> getFeedbackIdsByDateRange(String startDate, String endDate, String feedbackType) {
        List<String> feedbackIds = feedbackClient.getFeedbackIdsByDateRange(startDate,endDate,feedbackType);
        System.out.println("From service:" + feedbackIds);
        return feedbackIds;
    }

    @Override
    public void getFeedback(String feedbackId){
        FeedbackToIngest feedbackToIngest = feedbackClient.getFeedback(feedbackId);
        System.out.println("From service, feedback to ingest:" + feedbackToIngest);
    }
}
