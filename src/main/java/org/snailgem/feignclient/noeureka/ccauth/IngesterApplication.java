package org.snailgem.feignclient.noeureka.ccauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.util.StringUtils;

import java.util.Arrays;

@SpringBootApplication
@EnableFeignClients
public class IngesterApplication implements ApplicationRunner {

	@Autowired
	private IngesterService ingesterService;

	@Value("${start_date:}")
	private String startDate;
	@Value("${end_date:}")
	private String endDate;
	@Value("${feedbacktype:}")
	private String feedbackType;

	@Value("${feedbackid:}")
	private String feedbackId;

	public static void main(String[] args) {
		SpringApplication.run(IngesterApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) {
		System.out.println("started with arguments: " + Arrays.toString(args.getSourceArgs()));
		if (!StringUtils.isEmpty(startDate)) {
			ingesterService.getFeedbackIdsByDateRange(startDate, endDate, feedbackType);
		}
		if (!StringUtils.isEmpty(feedbackId)) {
			ingesterService.getFeedback(feedbackId);
		}
	}


}
