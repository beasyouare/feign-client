package org.snailgem.feignclient.noeureka.ccauth;

import java.util.List;

public interface IngesterService {

    List <String> getFeedbackIdsByDateRange(String startDate, String endDate, String feedbackType);

    void getFeedback(String feedbackId);

}