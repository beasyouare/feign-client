package org.snailgem.feignclient.noeureka.ccauth;

import org.snailgem.feignclient.ccauth.feedback.dto.FeedbackToIngest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name="feedbackClient", url="apps.dev-snapshot.clarivate.com/api")
public interface FeedbackClient {

    @RequestMapping(method = RequestMethod.GET, value="/authorship/feedback/extract/ids")
    List <String> getFeedbackIdsByDateRange(@RequestParam("startDate") String startDate,
                                            @RequestParam("endDate") String endDate,
                                            @RequestParam("feedbackType") String feedbackType);

    @RequestMapping(method = RequestMethod.GET, value="/authorship/feedback/extract/single/{feedbackId}")
    FeedbackToIngest getFeedback(@RequestParam("feedbackId") String feedbackId);

}