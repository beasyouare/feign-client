package org.snailgem.feignclient.noeureka.spresolver;

import java.util.List;

public interface IngesterService {

    void resolve(String truid);
}