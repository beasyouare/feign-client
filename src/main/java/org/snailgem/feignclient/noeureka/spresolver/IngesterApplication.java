package org.snailgem.feignclient.noeureka.spresolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.util.StringUtils;

import java.util.Arrays;

@SpringBootApplication
@EnableFeignClients
public class IngesterApplication implements ApplicationRunner {

	@Autowired
	private IngesterService ingesterService;

	@Value("${truid:}")
	private String truid;

	public static void main(String[] args) {
		SpringApplication.run(IngesterApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) {
		System.out.println("started with arguments: " + Arrays.toString(args.getSourceArgs()));
		if (!StringUtils.isEmpty(truid)) {
			ingesterService.resolve(truid);
		}
	}


}
