package org.snailgem.feignclient.noeureka.spresolver;

import feign.FeignException;
import org.snailgem.feignclient.spresolver.dto.ResolvedSPProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IngesterServiceImpl implements IngesterService {
    @Autowired
    private SpResolverClient spResolverClient;

    @Override
    public void resolve(String truid){
        try {
            ResolvedSPProfile resolvedSPProfile = spResolverClient.resolve(truid);
            System.out.println("From service, resolved:" + resolvedSPProfile);
        } catch (FeignException e) {
            System.out.println("Exception caught:" + e.getMessage());
            System.out.println("Exception status:" + e.status()); //use this to handle 404
            System.out.println("Exception content:" + e.contentUTF8());
        }
    }
}
