package org.snailgem.feignclient.spresolver.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResolvedSPProfile {

    private Integer spUniqueId;
    private String confidence;
    private boolean claimed;
    private String primaryName;
    private String publonsTruid;
    private String preferredRid;
    private List <String> rids;


    public Integer getSpUniqueId() {
        return spUniqueId;
    }

    public void setSpUniqueId(Integer spUniqueId) {
        this.spUniqueId = spUniqueId;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    public boolean isClaimed() {
        return claimed;
    }

    public void setClaimed(boolean claimed) {
        this.claimed = claimed;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getPublonsTruid() {
        return publonsTruid;
    }

    public void setPublonsTruid(String publonsTruid) {
        this.publonsTruid = publonsTruid;
    }

    public String getPreferredRid() {
        return preferredRid;
    }

    public void setPreferredRid(String preferredRid) {
        this.preferredRid = preferredRid;
    }

    public List <String> getRids() {
        return rids;
    }

    public void setRids(List <String> rids) {
        this.rids = rids;
    }

    @Override
    public String toString() {
        return "ResolvedSPProfile{" +
                "spUniqueId=" + spUniqueId +
                ", confidence='" + confidence + '\'' +
                ", claimed=" + claimed +
                ", primaryName='" + primaryName + '\'' +
                ", publonsTruid='" + publonsTruid + '\'' +
                ", preferredRid='" + preferredRid + '\'' +
                ", rids=" + rids +
                '}';
    }
}
