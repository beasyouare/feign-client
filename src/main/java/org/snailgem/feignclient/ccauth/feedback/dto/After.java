package org.snailgem.feignclient.ccauth.feedback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class After {

	@JsonProperty("Record")
	private Record record;

	public void setRecord(Record record){
		this.record = record;
	}

	public Record getRecord(){
		return record;
	}

	@Override
 	public String toString(){
		return 
			"After{" +
			"record = '" + record + '\'' + 
			"}";
		}
}