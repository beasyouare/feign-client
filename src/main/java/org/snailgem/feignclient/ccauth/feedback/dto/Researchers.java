package org.snailgem.feignclient.ccauth.feedback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Researchers {

	@JsonProperty("Researcher")
	private Researcher researcher;

	public void setResearcher(Researcher researcher){
		this.researcher = researcher;
	}

	public Researcher getResearcher(){
		return researcher;
	}

	@Override
 	public String toString(){
		return 
			"Researchers{" +
			"researcher = '" + researcher + '\'' + 
			"}";
		}
}