package org.snailgem.feignclient.ccauth.feedback.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Publications {

	@JsonProperty("After")
	private List<After> after;

	@JsonProperty("Excluded")
	private List<Excluded> excluded;

	public void setAfter(List<After> after){
		this.after = after;
	}

	public List<After> getAfter(){
		return after;
	}

	public void setExcluded(List<Excluded> excluded){
		this.excluded = excluded;
	}

	public List<Excluded> getExcluded(){
		return excluded;
	}

	@Override
	public String toString() {
		return "Publications{" +
				"after=" + after +
				", excluded=" + excluded +
				'}';
	}
}