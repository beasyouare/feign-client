package org.snailgem.feignclient.ccauth.feedback.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Researcher{

	@JsonProperty("RIDs")
	private List<String> rIDs;

	@JsonProperty("UserName")
	private String userName;

	@JsonProperty("TRUIDs")
	private List<String> tRUIDs;

	@JsonProperty("UserAdditionalInfo")
	private String userAdditionalInfo;

	@JsonProperty("Publications")
	private List<Publications> publications;

	@JsonProperty("UserId")
	private String userId;

	@JsonProperty("ModDate")
	private String modDate;

	@JsonProperty("FeedbackId")
	private String feedbackId;

	@JsonProperty("UserEmail")
	private String userEmail;

	@JsonProperty("UserRelation")
	private String userRelation;

	public void setRIDs(List<String> rIDs){
		this.rIDs = rIDs;
	}

	public List<String> getRIDs(){
		return rIDs;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setTRUIDs(List<String> tRUIDs){
		this.tRUIDs = tRUIDs;
	}

	public List<String> getTRUIDs(){
		return tRUIDs;
	}

	public void setUserAdditionalInfo(String userAdditionalInfo){
		this.userAdditionalInfo = userAdditionalInfo;
	}

	public String getUserAdditionalInfo(){
		return userAdditionalInfo;
	}

	public void setPublications(List<Publications> publications){
		this.publications = publications;
	}

	public List<Publications> getPublications(){
		return publications;
	}

	@JsonIgnore
	public List getAfterItems () {
		for (Publications p : this.getPublications()){
			if (!CollectionUtils.isEmpty(p.getAfter())){
				return (p.getAfter());
			}
		}
		return Collections.EMPTY_LIST;
	}

	@JsonIgnore
	public List<Excluded> getExcludedItems () {
		for (Publications p : this.getPublications()){
			if (!CollectionUtils.isEmpty(p.getExcluded())){
				return (p.getExcluded());
			}
		}
		return Collections.<Excluded>emptyList();
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setModDate(String modDate){
		this.modDate = modDate;
	}

	public String getModDate(){
		return modDate;
	}

	public void setFeedbackId(String feedbackId){
		this.feedbackId = feedbackId;
	}

	public String getFeedbackId(){
		return feedbackId;
	}

	public void setUserEmail(String userEmail){
		this.userEmail = userEmail;
	}

	public String getUserEmail(){
		return userEmail;
	}

	public void setUserRelation(String userRelation){
		this.userRelation = userRelation;
	}

	public String getUserRelation(){
		return userRelation;
	}

	@Override
 	public String toString(){
		return 
			"Researcher{" + 
			"rIDs = '" + rIDs + '\'' + 
			",userName = '" + userName + '\'' + 
			",tRUIDs = '" + tRUIDs + '\'' + 
			",userAdditionalInfo = '" + userAdditionalInfo + '\'' + 
			",publications = '" + publications + '\'' + 
			",userId = '" + userId + '\'' + 
			",modDate = '" + modDate + '\'' + 
			",feedbackId = '" + feedbackId + '\'' + 
			",userEmail = '" + userEmail + '\'' + 
			",userRelation = '" + userRelation + '\'' + 
			"}";
		}
}