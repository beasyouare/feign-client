package org.snailgem.feignclient.ccauth.feedback.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeedbackToIngest{

	@JsonProperty("Researchers")
	private List<Researchers> researchers;

	public void setResearchers(List<Researchers> researchers){
		this.researchers = researchers;
	}

	public List<Researchers> getResearchers(){
		return researchers;
	}

	@JsonIgnore
	public Researcher getResearcher () {
		return this.getResearchers().get(0).getResearcher();
	}

	@Override
 	public String toString(){
		return 
			"FeedbackToIngest{" + 
			"researchers = '" + researchers + '\'' + 
			"}";
		}
}