package org.snailgem.feignclient.ccauth.feedback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Excluded {

	@JsonProperty("Record")
	private Record record;

	public void setRecord(Record record){
		this.record = record;
	}

	public Record getRecord(){
		return record;
	}

	@Override
 	public String toString(){
		return 
			"Excluded{" +
			"record = '" + record + '\'' + 
			"}";
		}
}