package org.snailgem.feignclient.ccauth.feedback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Record{

	@JsonProperty("UID")
	private String uID;

	@JsonProperty("Position")
	private int position;

	public void setUID(String uID){
		this.uID = uID;
	}

	public String getUID(){
		return uID;
	}

	public void setPosition(int position){
		this.position = position;
	}

	public int getPosition(){
		return position;
	}

	@Override
 	public String toString(){
		return 
			"Record{" + 
			"uID = '" + uID + '\'' + 
			",position = '" + position + '\'' + 
			"}";
		}
}