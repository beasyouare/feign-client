package org.snailgem.feignclient.eureka.spresolver;

import org.snailgem.feignclient.spresolver.dto.ResolvedSPProfile;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="SPMASTERSERVICES")
public interface SpResolverClient {

    @RequestMapping(method = RequestMethod.GET, value="/spmaster/resolution/truid/{truid}")
    ResolvedSPProfile resolve(@RequestParam("truid") String truid);

}