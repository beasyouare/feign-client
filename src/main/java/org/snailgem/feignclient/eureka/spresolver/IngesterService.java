package org.snailgem.feignclient.eureka.spresolver;

public interface IngesterService {

    void resolve(String truid);
}