package org.snailgem.feignclient.eureka.spresolver;

import org.snailgem.feignclient.spresolver.dto.ResolvedSPProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IngesterServiceImpl implements IngesterService {
    @Autowired
    private SpResolverClient spResolverClient;

    @Override
    public void resolve(String truid){
        ResolvedSPProfile resolvedSPProfile;
        try {
            resolvedSPProfile = spResolverClient.resolve(truid);
            System.out.println("Resolved profile for truid " + truid + ":" + resolvedSPProfile);
        } catch (IllegalStateException e) {
            System.out.println("Caught IllegalStateException:" + e.getMessage());
        }
    }
}
