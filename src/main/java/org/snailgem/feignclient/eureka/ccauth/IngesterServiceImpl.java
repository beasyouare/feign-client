package org.snailgem.feignclient.eureka.ccauth;

import org.snailgem.feignclient.ccauth.feedback.dto.FeedbackToIngest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngesterServiceImpl implements IngesterService {
    @Autowired
    private FeedbackClient feedbackClient;

    @Override
    public List <String> getFeedbackIdsByDateRange(String startDate, String endDate, String feedbackType) {
        List<String> feedbackIds = feedbackClient.getFeedbackIdsByDateRange(startDate,endDate,feedbackType);
        System.out.println("Feedback ids:" + feedbackIds);
        return feedbackIds;
    }

    @Override
    public void getFeedback(String feedbackId){
        try {
            FeedbackToIngest feedbackToIngest = feedbackClient.getFeedback(feedbackId);
            System.out.println("Feedback to ingest:" + feedbackToIngest);
        }
        catch (IllegalStateException e) {
            System.out.println("Caught IllegalStateException:" + e.getMessage());
        }
    }
}
